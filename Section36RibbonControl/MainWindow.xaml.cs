﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Ribbon;

namespace Section36RibbonControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CopyButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("I am Copy Ribbon Button");
        }

        private void CutButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("I am Cut Ribbon Button");
        }

        private void PasteButton_Checked(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("I am Checked with Paste Button");
            if (PasteButton.IsChecked)
            {

            }
        }

        private void PasteButton_Unchecked(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("I am un Checked with Paste Button");
            RibbonMenuItem item = new RibbonMenuItem();
            
        }
    }
}
