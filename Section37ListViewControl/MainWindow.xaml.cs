﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Section37ListViewControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Adding items Dynamically to ListView Data using textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddListViewButton_Click(object sender, RoutedEventArgs e)
        {
            if (txtListView.Text != string.Empty)
            {
                MainListView.Items.Add(txtListView.Text);
                txtListView.Text = "";
                lblCount.Content = MainListView.Items.Count;
            }
        }

        /// <summary>
        /// Delete the Selected ListView Item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteListViewButton_Click(object sender, RoutedEventArgs e)
        {
            MainListView.Items.Remove(MainListView.SelectedItem);
            lblCount.Content = MainListView.Items.Count;
        }

        /// <summary>
        /// Setting the ListView Items Count
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblCount.Content = MainListView.Items.Count;
        }

        /// <summary>
        /// Deleting the All ListView Items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteAllListViewButton_Click(object sender, RoutedEventArgs e)
        {
            MainListView.Items.Clear(); 
            lblCount.Content = MainListView.Items.Count;
        }

        /// <summary>
        /// Adding the ListView Items using List.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddListvaluestoListViewButton_Click(object sender, RoutedEventArgs e)
        {
            //Adding List Values to ListView Items
            List<string> list = new List<string>();
            list.Add("Male");
            list.Add("Female");
            list.Add("Others");
            secondListView.ItemsSource = list;
        }

        /// <summary>
        /// Loading the ListView Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadListViewButton_Click(object sender, RoutedEventArgs e)
        {
            string imgPath = Environment.CurrentDirectory + "\\Photos\\";
            var result = Directory.EnumerateFiles(imgPath, "*.png");

            foreach (var file in result)
            {
                ListViewItem listViewItem = new ListViewItem();
                StackPanel stackPanel = new StackPanel();
                Image image = new Image();
                Label label = new Label();

                FileInfo info = new FileInfo(file);

                Uri uri = new Uri(file);
                image.Source = new BitmapImage(uri);
                image.Width = 30;
                image.Height = 30;
                label.Content = info.Name;

                stackPanel.Orientation = Orientation.Horizontal;
                stackPanel.Children.Add(image);
                stackPanel.Children.Add(label);
                listViewItem.Content = stackPanel;

                ImageListViewCSharp.Items.Add(listViewItem);
            }
        }
    }    
}
