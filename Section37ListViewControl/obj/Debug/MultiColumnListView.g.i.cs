﻿#pragma checksum "..\..\MultiColumnListView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "3801103F609F3B90564FE147C38F8D4BF310A8097F61FE1EDED903DAA1E6873B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Section37ListViewControl;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Section37ListViewControl {
    
    
    /// <summary>
    /// MultiColumnListView
    /// </summary>
    public partial class MultiColumnListView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 25 "..\..\MultiColumnListView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView MultiListView;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\MultiColumnListView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Add_multi_column_data_by_items;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\MultiColumnListView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Add_multi_column_data_by_List;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\MultiColumnListView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RefreshButton;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\MultiColumnListView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RemoveButton;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\MultiColumnListView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteAllButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Section37ListViewControl;component/multicolumnlistview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MultiColumnListView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MultiListView = ((System.Windows.Controls.ListView)(target));
            return;
            case 2:
            this.Add_multi_column_data_by_items = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\MultiColumnListView.xaml"
            this.Add_multi_column_data_by_items.Click += new System.Windows.RoutedEventHandler(this.Add_multi_column_data_by_items_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Add_multi_column_data_by_List = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.RefreshButton = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\MultiColumnListView.xaml"
            this.RefreshButton.Click += new System.Windows.RoutedEventHandler(this.RefreshButton_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.RemoveButton = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\MultiColumnListView.xaml"
            this.RemoveButton.Click += new System.Windows.RoutedEventHandler(this.RemoveButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.DeleteAllButton = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\MultiColumnListView.xaml"
            this.DeleteAllButton.Click += new System.Windows.RoutedEventHandler(this.DeleteAllButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

