﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Section37ListViewControl
{
    /// <summary>
    /// Interaction logic for MultiColumnListView.xaml
    /// </summary>
    public partial class MultiColumnListView : Window
    {
        public MultiColumnListView()
        {
            InitializeComponent();
        }

        int count = 1;
        private void Add_multi_column_data_by_items_Click(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;
            string imgPath = Environment.CurrentDirectory + "\\Photos\\";
            var result = Directory.EnumerateFiles(imgPath, "*.png");

            foreach (var file in result)
            {
                FileInfo info = new FileInfo(file);

                Uri uri = new Uri(file);

                DateTime date1 = new DateTime(2008, 5, 1, 8, 30, 52);
                MultiColumn multi = new MultiColumn(count++, new BitmapImage(uri), info.Name, info.CreationTime);
                MultiListView.Items.Add(multi);
            }
            Cursor = Cursors.Arrow;
        }
        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            MultiListView.Items.Refresh();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (MultiListView.SelectedItem != null)
                MultiListView.Items.Remove(MultiListView.SelectedItem);
            else if (MultiListView.Items.Count <= 0)
                MessageBox.Show("No files are existing", "File Information", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                MessageBox.Show("Please Select the File", "File Selection", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void DeleteAllButton_Click(object sender, RoutedEventArgs e)
        {
            count = 1;
            if (MultiListView.Items.Count > 0)
            {
                var result = MessageBox.Show("Do you want to delete all Files", "Delete All", MessageBoxButton.YesNo, MessageBoxImage.Error);

                if (result == MessageBoxResult.Yes)
                {
                    MultiListView.Items.Clear();
                }
            }
            else
            {
                MessageBox.Show("No files are existing", "File Information", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
    public class MultiColumn
    {
        private int sno;
        private string name;
        private string dateTime;
        private BitmapImage image;

        public int SNo
        {
            get { return sno; }
            set { value = sno; }
        }

        public BitmapImage Image
        {
            get { return image; }
            set { value = image; }
        }

        public string Name
        {
            get { return name; }
            set { value = name; }
        }

        public string DateTime
        {
            get { return dateTime; }
            set { value = dateTime; }
        }

        public MultiColumn(int sno, BitmapImage image, string name, DateTime dateTime)
        {
            this.sno = sno;
            this.image = image;
            this.name = name;
            this.dateTime = dateTime.ToString();
        }
    }
}
