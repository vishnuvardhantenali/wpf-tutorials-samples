﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Section17AdvancedBindings.TwoWayBinding
{
    /// <summary>
    /// Interaction logic for TwoWayBinding.xaml
    /// </summary>
    public partial class TwoWayBinding : Window
    {
        public TwoWayBinding()
        {
            InitializeComponent();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            BindingExpression bindingExpression = txtCntrlExplicit.GetBindingExpression(TextBox.TextProperty);
            bindingExpression.UpdateSource();
        }
    }
}
