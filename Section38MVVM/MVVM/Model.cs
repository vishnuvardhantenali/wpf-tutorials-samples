﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Section38MVVM.MVVM
{
    public class Model
    {    
        public string FirstName { get; set; }      
        public Model(string name)
        {
            FirstName = name;
        }        
    }
}
