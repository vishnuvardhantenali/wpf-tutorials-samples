﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Section25WindowEvents
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Window is loaded");
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show($"Keydown: {e.Key}");
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //MessageBox.Show("Window is closed");
        }

        private void ComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem comboBoxItem = new ComboBoxItem();
            comboBoxItem = (ComboBoxItem)ComboBox1.SelectedValue;
            lblCmbValue.Content = comboBoxItem.Content.ToString();
        }
    }
}
