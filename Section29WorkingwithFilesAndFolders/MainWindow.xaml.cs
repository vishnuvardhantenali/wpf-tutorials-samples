﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using Microsoft.Win32;

namespace Section29WorkingwithFilesAndFolders
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter ="Image Files|*.GIF;*.BMP;*.JPEG;*.PNG;*.JPG|All files (*.*)|*.*";
            openFile.ShowDialog();
            txtPath.Text = openFile.FileName;
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            FileCheck(string.Empty);
        }
        public void FileCheck(string temp)
        {
            bool fileExist = File.Exists(txtPath.Text);
            if (fileExist)
            {
                MessageBox.Show($"File is existing in the specified path");
            }
            else if (!fileExist && temp != string.Empty) 
            {
                MessageBox.Show($"File is {temp} in the specified path");
            }
            else
            {
                MessageBox.Show("File is not existing in the specified path");
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                File.Delete(txtPath.Text);

                FileCheck("Deleted");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string copyFrom = txtPathCopyFrom.Text;
                FileInfo copyFromFileInfo = new FileInfo(copyFrom);
                string copyFromFileExtention = copyFromFileInfo.Extension;
                string CopyFileName = copyFromFileInfo.Name;
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(copyFrom);

                string copyTo = copyFromFileInfo.Directory.Root.FullName + fileNameWithoutExt + "_Copy" + copyFromFileExtention;

                File.Copy(copyFrom, copyTo);
                string message = "Copied file Location: " + copyTo;
                string title = "Copied the file";
                MessageBox.Show(message, title);
            }
            catch
            {

            }
        }

        private void BtnBrowseCopy_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "All files (*.*)|*.*";
            openFile.ShowDialog();
            txtPathCopyFrom.Text = openFile.FileName;
        }

        private void BtnBrowseCopyFrom_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "All files (*.*)|*.*";
            openFile.ShowDialog();
            txtPathCopyFrom.Text = openFile.FileName;
        }

        private void BtnMove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string copyFrom = txtPathCopyFrom.Text;
                FileInfo copyFromFileInfo = new FileInfo(copyFrom);
                string copyFromFileExtention = copyFromFileInfo.Extension;
                string CopyFileName = copyFromFileInfo.Name;
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(copyFrom);


                string copyTo = copyFromFileInfo.Directory.Root.FullName + fileNameWithoutExt + "_Copy" + copyFromFileExtention;

                File.Move(copyFrom, copyTo);
                string message = "Copied file Location: " + copyTo;
                string title = "Copied the file";
                MessageBox.Show(message, title);
            }
            catch
            {

            }
        }

        private void BtnFolderCheck_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPathFolder.Text))
            {
                return;
            }
            bool FolderCheck=Directory.Exists(txtPathFolder.Text);
            string title = "Folder Confirmation";
            string message = string.Empty;
            if (FolderCheck)
            {
                message = "Folder is Exists: "+txtPathFolder.Text;
                MessageBox.Show(message, title,MessageBoxButton.OK,MessageBoxImage.Information);
            }
            else
            {
                message = "Folder is not Exists: " + txtPathFolder.Text+"\nDo you want to create";
                MessageBoxResult result= MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Error);
                if (result == MessageBoxResult.Yes)
                {
                    Directory.CreateDirectory(txtPathFolder.Text);
                }
                else
                    return;
            }            
        }

        private void BtnFolderDelete_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPathFolder.Text))
            {
                return;
            }
            bool FolderCheck = Directory.Exists(txtPathFolder.Text);
            string title = "Folder Delete Confirmation";
            string message = string.Empty;
            if (FolderCheck)
            {
                message = "Folder is Exists: " + txtPathFolder.Text;
                MessageBoxResult result = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (result == MessageBoxResult.Yes)
                {
                    Directory.Delete(txtPathFolder.Text);
                }
                else
                    return;
            }
            else
            {
                message = "Folder is not Exists: " + txtPathFolder.Text;
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
