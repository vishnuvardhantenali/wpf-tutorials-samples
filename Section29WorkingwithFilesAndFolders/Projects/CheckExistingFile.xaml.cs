﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Section29WorkingwithFilesAndFolders.Projects
{
    /// <summary>
    /// Interaction logic for CheckExistingFile.xaml
    /// </summary>
    public partial class CheckExistingFile : Window
    {
        public CheckExistingFile()
        {
            InitializeComponent();
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {

            FileSerach();
        }
        public void FileSerach()
        {
            bool fileExists = File.Exists(txtPath.Text);
            if (fileExists)
            {
                MessageBox.Show("File is exits");
            }
            else
            {
                MessageBox.Show("File is not exits");
            }
        }

        private void BtnSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                FileSerach();
            }
        }
    }
}
