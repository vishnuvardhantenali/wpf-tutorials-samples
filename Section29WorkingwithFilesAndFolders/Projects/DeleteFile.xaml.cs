﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Section29WorkingwithFilesAndFolders.Projects
{
    /// <summary>
    /// Interaction logic for DeleteFile.xaml
    /// </summary>
    public partial class DeleteFile : Window
    {
        public DeleteFile()
        {
            InitializeComponent();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            bool fileExists = File.Exists(txtPath.Text);

            if (fileExists)
            {
                File.Delete(txtPath.Text);
                MessageBox.Show("File is exits and Deleted");
            }
            else
            {
                MessageBox.Show("File is not exits");
            }
        }
    }
}
