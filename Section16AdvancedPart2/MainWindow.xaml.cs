﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Section16AdvancedPart2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
            //find out the window dimentions
            
            var width=System.Windows.SystemParameters.PrimaryScreenWidth;
        }

        private void SliderControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SliderLabel.Content = SliderControl.Value.ToString();
            SliderButtonCheck.Width = SliderControl.Value;
        }

        private void CheckBoxCSharp_Checked(object sender, RoutedEventArgs e)
        {
            ButtonCSharp.IsEnabled = CheckBoxCSharp.IsChecked.Value?false:true;
            ButtonShowHide.Visibility = CheckBoxCSharp.IsChecked.Value ? Visibility.Visible : Visibility.Hidden;
            //MessageBox.Show("Checked");
        }

        private void CheckBoxCSharp_Unchecked(object sender, RoutedEventArgs e)
        {
            //ButtonCSharp.IsEnabled = true;
            ButtonCSharp.IsEnabled = CheckBoxCSharp.IsChecked.Value ? false : true;
            ButtonShowHide.Visibility = CheckBoxCSharp.IsChecked.Value ? Visibility.Visible : Visibility.Hidden;
            //MessageBox.Show("Un-Checked");
        }

        private void ButtonCSharp_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("I am C# Button ");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var t=(pswdText.Password);
            MessageBox.Show(t);
        }

        private void RadioButtonMale_Checked(object sender, RoutedEventArgs e)
        {
            RadioButtonTextBox.Text = "Male";
        }

        private void RadioButtonFemale_Checked(object sender, RoutedEventArgs e)
        {
            RadioButtonTextBox.Text = "Female";
            //..MessageBox.Show(RadioButtonFemale.Content.ToString());
        }

        private void RadioButtonOthers_Checked(object sender, RoutedEventArgs e)
        {
            RadioButtonTextBox.Text = "Others";
            // MessageBox.Show(RadioButtonOthers.IsChecked.ToString());
        }

        public void Gender()
        {
            RadioButtonTextBox.Text = RadioButtonMale.ContentStringFormat;
        }

        private void ProgressSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ProgressControl.Value = ProgressSlider.Value;
            ProgressLabel.Content= ProgressSlider.Value+" %";
            if (ProgressSlider.Value >= 50)
            {
                ProgressLabel.Foreground = Brushes.White;
            }
            else
            {
                ProgressLabel.Foreground = Brushes.Black;
            }

        }

        private void ButtonUp_Click(object sender, RoutedEventArgs e)
        {
            ProgressControl.Value = ProgressControl.Value +1;
            ProgressLabel.Content = ProgressControl.Value+" %";
            ProgressSlider.Value = ProgressControl.Value;
        }

        private void ButtonDown_Click(object sender, RoutedEventArgs e)
        {
            ProgressControl.Value = ProgressControl.Value - 1;
            ProgressLabel.Content = ProgressControl.Value+" %";
            ProgressSlider.Value = ProgressControl.Value;
        }

        private void ProgressControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //MessageBox.Show("I am chanhjvju");
        }
    }
}
