﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Child_Window child = new Child_Window();
            child.btnChildWindowYes.Visibility = Visibility.Hidden;
            child.btnChildWindowNo.Visibility = Visibility.Hidden;
            child.Show();
        }

        private void BtnChildDialog_Click(object sender, RoutedEventArgs e)
        {
            Child_Window child = new Child_Window();
            child.ShowDialog();
        }

        private void BtnChildDialogPrperties_Click(object sender, RoutedEventArgs e)
        {
            Child_Window child = new Child_Window();
            child.FontSize = 200;
            child.Title = "I am Changing the child properties";
            child.Background = Brushes.BlueViolet;
            child.Show();
        }
    }
}
