﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFEvents
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button.FontSize = 15;
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Window Loaded");
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button.FontSize = 12;
        }

        private void Button_KeyDown(object sender, KeyEventArgs e)
        {
            
        }


        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            // MessageBox.Show(e.Key.ToString());
            MainWindow1.Title = e.Key.ToString();
            if (e.SystemKey == Key.F10)
            {
                BtnSave_Click(sender, e);
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Save Button Clicked");
        }

        private void TxtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            MainWindow1.Title = txtBox.Text;
        } 

        private void TxtBox_GotFocus(object sender, RoutedEventArgs e)
        {
            txtBox.Background = Brushes.Aqua;
        }

        private void TxtBox_LostFocus(object sender, RoutedEventArgs e)
        {
            txtBox.Background = Brushes.White;
        }

        private void BtnSave_MouseEnter(object sender, MouseEventArgs e)
        {
            btnSave.Background = Brushes.Yellow;
        }

        private void BtnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            btnSave.Background = Brushes.Green;
        }

        private void SeklectStateButton_Click(object sender, RoutedEventArgs e)
        {
            var t=statecmb.SelectedIndex ;
            txtBox.Text=(t.ToString());
        }

        private void Statecmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var t = statecmb.SelectedIndex;
            txtBox.Text = (t.ToString());
            //var t = statecmb.SelectionBoxItem;
            //MessageBox.Show(t.ToString());
        }

        private void SetHeighListBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            NameListBox.Items.Add(DynamicalAddList.Text);
            //var t= Resources["vishnuListBox"];// = 25;//Convert.ToInt32(setHeighListBox.Text);
            //Resources["vishnuListBox"]= (Double)35;
            //MessageBox.Show(t.ToString());
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {            
            if (NameListBox.SelectedItem != null)
            {
                Undo();
                NameListBox.Items.RemoveAt(NameListBox.Items.IndexOf(NameListBox.SelectedItem));
                
            }
        }

        private void UndoButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(y.ToString());
        }
        public object y;
        public void Undo()
        {
            var t=(ListBoxItem)(NameListBox.SelectedItem);
            y = NameListBox.SelectedItem;
        }

        private void NameListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MessageBox.Show(NameListBox.SelectedItem.ToString());
        }
    }
}
