﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Section35CommonControls3.ProControls
{
    /// <summary>
    /// Interaction logic for TabControls.xaml
    /// </summary>
    public partial class TabControls : Window
    {
        public TabControls()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItem typeItem = (ComboBoxItem)cmbSettingSelections.SelectedItem;
            string value = typeItem.Content.ToString();

            MessageBox.Show(txtAplTextBox.Text+"\n"+value+"\n"+cmbDisplay.SelectedValue);
        }
    }
}
