﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Section20AdvancedMultiWindowApps
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Display the Child Window from parent window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowChild_Click(object sender, RoutedEventArgs e)
        {
            ChildWindow childWindow = new ChildWindow();    
            childWindow.Show();
        }

        /// <summary>
        /// Display child window as a Dialog.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowChildDialog_Click(object sender, RoutedEventArgs e)
        {
            ChildWindow childWindow = new ChildWindow();
            childWindow.ShowDialog();
        }

        /// <summary>
        /// Change the Child window and control properties from parent window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowChildProperties_Click(object sender, RoutedEventArgs e)
        {
            ChildWindow childWindow = new ChildWindow();

            //Change the Child window Properties from parent window.
            childWindow.Background = Brushes.Yellow;

            //Change the Child control properties from parent window.
            childWindow.lblName.Foreground = Brushes.Red;
            childWindow.lblName.FontWeight = FontWeights.Bold;

            childWindow.Show();
        }
    }
}
