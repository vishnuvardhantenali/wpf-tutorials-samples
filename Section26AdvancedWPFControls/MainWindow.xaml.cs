﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Section26AdvancedWPFControls
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            btnSample.IsEnabled = false;
        }

        private void sldControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblValue.Content = sldControl.Value;
        }

        private void chkShowHide_Checked(object sender, RoutedEventArgs e)
        {
            if (btnSample != null)
            {
                btnSample.Visibility = Visibility.Visible;
            }
        }

        private void chkShowHide_Unchecked(object sender, RoutedEventArgs e)
        {
            btnSample.Visibility = Visibility.Collapsed;
        }

        private void chkEnable_Checked(object sender, RoutedEventArgs e)
        {
            btnSample.IsEnabled = true;
        }

        private void chkEnable_Unchecked(object sender, RoutedEventArgs e)
        {
            btnSample.IsEnabled ^= true;
        }

        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(pswdControl.Password);
        }
    }
}
